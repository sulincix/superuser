#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>

pid_t shell_pgid;
struct termios shell_tmodes;
int shell_terminal;
int shell_is_interactive;

/*
Create shell source is part of GNU:
https://ftp.gnu.org/old-gnu/Manuals/glibc-2.2.3/html_chapter/libc_27.html
 */
void init_shell () {

  shell_terminal = STDIN_FILENO;
  shell_is_interactive = isatty (shell_terminal);

  if (shell_is_interactive) {
      /* Loop until we are in the foreground.  */
      while (tcgetpgrp (shell_terminal) != (shell_pgid = getpgrp ()))
        kill (- shell_pgid, SIGTTIN);

      /* Ignore interactive and job-control signals.  */
      signal (SIGINT, SIG_IGN);
      signal (SIGQUIT, SIG_IGN);
      signal (SIGTSTP, SIG_IGN);
      signal (SIGTTIN, SIG_IGN);
      signal (SIGTTOU, SIG_IGN);
      signal (SIGCHLD, SIG_IGN);

      /* Put ourselves in our own process group.  */
      shell_pgid = getpid ();
      if (setpgid (shell_pgid, shell_pgid) < 0)
        {
          perror ("Couldn't put the shell in its own process group");
          exit (1);
        }

      /* Grab control of the terminal.  */
      tcsetpgrp (shell_terminal, shell_pgid);

      /* Save default terminal attributes for shell.  */
      tcgetattr (shell_terminal, &shell_tmodes);
    }
}

void main(int argc, char** argv){
    if(argc<2){
        exit(1);
    }
    /*  Define fds */
    char fd[1024];

    init_shell();

    /* stdout */
    sprintf(fd,"/proc/%s/fd/%d",argv[1], 1);
    dup2(open(fd ,O_WRONLY | O_APPEND),1);
    /* stderr */
    sprintf(fd,"/proc/%s/fd/%d",argv[1], 2);
    dup2(open(fd ,O_WRONLY | O_APPEND),2);
    /* stdin */
    sprintf(fd,"/proc/%s/fd/%d",argv[1], 0);
    dup2(open(fd,O_RDONLY | O_APPEND),0);


    system("/bin/sh");
}

